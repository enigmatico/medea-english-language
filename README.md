# Medea-English-Language

Pattern files for the [Medea Engine](https://gitlab.com/enigmatico/medea) that teaches the program how to speak.

## Writting pattern files (pyb files)

The engine will look at every .pyb file and process them. Once processed, it will generate a dictionary in memory with all the pattern and their respective
responses.

These .pyb files are in XML format, in a similar fashion on how AIML work.

## The pyb format

Each pyb file is divided into different XML sections (as tags), starting with the root of the document (The pybot element):

· __Concept__ : Includes a parameter 'Name' with a descriptive name for that pattern.

Inside the Concept element, the following must be included:

· __Default__ : MUST BE DEFINED ONLY ONCE AND ONLY IN ONE FILE. This is the default pattern that will be called when the bot can't find a matching pattern for the input. It's format is the same as Model.

· __Model__ : A model that defines a pattern. Each pattern file will include several models for each pattern.

Each model can contain the following elements:

· __Pattern__ : The actual pattern. The input text will be compared against this pattern to see if they match. If several matching patterns are found, their weights will be calculated and the one with the highest weight will be used. The weights are calculated based on the number of words in the pattern and their wildcards.

· __Response__ : The response for the pattern. When the bot checks what patterns matches, it returns the response from the pattern with the highest weight.

· __Random__ : This element replaces Response. If the Random element is specified instead of a Response, the bot will pick up a random response from a list of responses.

The Random tag contains the following tags:

· __Item__ : A response element that goes inside a Random element. Several Item elements can be defined inside the Random element. Each Item element contains a response string.

## Pattern wildcards:

Wildcards can be used in the Pattern elements as placeholders for when more than one input can be matched with the pattern. I.E if we wanted to use the same answer for when the input is "TODAY IS (something)" then we can set the pattern to "TODAY IS \*", and this rule will apply for "TODAY IS MONDAY", "TODAY IS TUESDAY" or even "TODAY IS A BEAUTIFUL DAY".

Wildcards also modify the weight of your patterns so be aware of this.

__NAME__ ( character ) ( weight value )

· __MATCH ONE OR MORE__ ( __\*__ ) ( 2 ) : This wildcard will match ANY sucession of words, but at least one word.

· __MATCH ONE__ ( __\___ ) ( 4 ) : This wildcard will match ONLY ONE word.

· __MATCH ONE OR NONE__ ( __?__ ) ( 6 ) : This wildcard will match one word, IF there is a word in that place.

· __MATCH ANY__ ( __^__ ) ( 1 ) : This wildcard will match ANYTHING in that position, or nothing if there is nothing.

· __MATCH ONE WORD__ ( ( ... | ... ) ) ( 1 ) : This wildcard allows you to specify a list of words, and it will match ANY of the words inside, but at least ONE word. i.e: (FOOTBALL | BASKETBALL | BASEBALL) to match either FOOTBALL, BASKETBALL OR BASEBALL.

NOTE: Regular words have a weight of 1 each. The total weight of a pattern is the sum of the word, plus the value of all the patterns.

## Bot Constants and variables

The bot engine has support for the use of constants, and variables. Constants are defined in the AI.json file, and can not be changed. Variables can be defined and set during run time. It is possible to use the value of the constants and variables in your response patterns:

· __Constants__ : To retrieve the value of a constant value, use the $[] operator in your Response or Item elements. i.e: $[master] will return the value of the constant 'master'. Constants are defined in the AI.json file.

· __Get the value from a wildcard__ : You can retrieve the value from a wildcard with the [::] operator. The value represents the position of the wildcard you want to read. i.e: If the Response entity is YOUR NAME IS [:0:], the pattern is 'MY NAME IS \*' and the input is 'My name is Andrew', the output will be 'YOUR NAME IS Andrew'.

· __Get a variable__ : You can get a variable with the @[] operator in your Response or Item elements. By general rule, you'll want to get a variable whose name is in the input text. For this, you'll need to set a wildcard in your pattern (like \*), and then get the word or words obtained with the wildcard with the @[ [::] ] operator, specifying the position of the wildcard to read. i.e: 'Today is @[day]' will return whatever the variable 'day' is set to (if it exists). But if the answer is 'Today is not @[ [:0:] ]', it will replace @[ [:0:] ] with a variable whose name is whatever text is obtained from the first wildcard. If the pattern is 'TODAY IS \*' and the input is 'TODAY IS MONDAY', then the output will be the value of the variable 'monday'. If monday = tuesday then the final output would be 'Today is not tuesday'.

If the variable is not set then the output will be left blank.

· __Set a variable__ : In your Response or Item elements, you can reference your variables with the @[ = ] operator. i.e: @[ day=[:0:] ] will set a variable named 'day' to the value obtained by the first wildcard. If the pattern was 'TODAY IS \*' and the input was 'Today is Monday', the variable 'day' would be set to 'monday'.

You can also use the @[ [::]=[::] ] operator if you also want the name of the variable to be obtained from the Pattern. i.e: @[ [:0:] = [:1:] ] for the pattern 'I AM \* SOMETHING \*' with the input 'I AM DOING SOMETHING GOOD' will set a variable named 'doing' with the value 'good'.

Variables don't need to exist, they will be created on runtime when necessary.

## Conditionals and functions, Mapping

These are planned functionalities, but are not yet fully implemented.